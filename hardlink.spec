name:     hardlink
Version:  1.0
Release:  24
Epoch:    1
Summary:  A tool used to create a tree of hard links
License:  GPL+
URL:      https://pagure.io/hardlink
Source0:  hardlink.c
Source1:  hardlink.1

Obsoletes: kernel-utils

%description
Hardlink is used to create a hard link tree.
The kernel installs it to significantly reduce the
amount of disk space used by each kernel package installed.

%package_help

%prep
%setup -q -c -T
install -pm 644 %{S:0} hardlink.c

%build
gcc %optflags hardlink.c -o hardlink

%install
rm -rf %{buildroot}
install -D -m 644 -t %{buildroot}%{_mandir}/man1   %{S:1}
install -D -m 755 -t %{buildroot}%{_sbindir} hardlink

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%{_sbindir}/hardlink

%files help
%{_mandir}/man1/*.gz

%changelog
* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.0-24
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded patch

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.0-23
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise description

* Mon Aug 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.0-22
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:strengthen spec

* Tue Aug 20 2019 fangxiuning<fangxiuning@huawei.com> - 1:1.0-21
- Type:NA
- ID:NA
- SUG:NA
- DESC:Rewrite Spec File

* Sat Dec 29 2018 yujinyang <yujinyang1@huawei.com> - 1:1.0-20
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fixed 32 bit build with gcc7

* Fri Jan 24 2014 openEuler Buildteam <buildteam@openeuler.org> - 1:1.0-19
- Package init
